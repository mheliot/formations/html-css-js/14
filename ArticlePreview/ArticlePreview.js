export default {

    name: 'ArticlePreview',
    template: `
        <aside class="article-preview">
            <slot></slot>
        </aside>
    `
    
};