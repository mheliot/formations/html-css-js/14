import ArticlePreview from './ArticlePreview/ArticlePreview.js';
import Book from './Book/Book.js';

// Application
export default {

    data() {
        return {
            title: 'Gestion des articles',
            article: null,
            articles: []
        }
    },

    methods: {        

        addArticle(article) {
            this.articles.push(article)
        },

        setArticle(article) {
            this.article = article
        },

        closeModale() {
            this.$refs.addModale.close()
        },

        openModale() {
            this.$refs.addModale.showModal()
        },

        handleAddForm() {
            var data = new FormData(this.$refs.addForm)
            var article = {};
            data.forEach(function(value, key) {
                article[key] = value;
            });
            article.createdAt = new Date()
            this.addArticle(article)
            this.closeModale()
            this.$refs.addForm.reset()
        }
    },

    created() {
        fetch('http://my-json-server.typicode.com/mathieuheliot/courses-api/books')
            .then( response => response.json() )
            .then( data => this.articles = data )
            .catch( error => console.error(error) )
    },

    components: {
        ArticlePreview,
        Book
    }
}